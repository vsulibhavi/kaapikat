package com.emoticup.kaapikat;

import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;


public class EmoticupRegistration {

    private String registeredId = "";

    public static final MediaType MEDIA_TYPE_JSON
            = MediaType.parse("application/json; charset=utf-8");

    private static EmoticupRegistration emoticupRegistration  = new EmoticupRegistration();
    private final OkHttpClient client = new OkHttpClient();

    public  EmoticupRegistrationResponse registerUser(RegisterUser registerUser ){
        Request request = new Request.Builder()
                .url("https://kaapi.eu-gb.mybluemix.net/registerUser")
                .post(RequestBody.create(MEDIA_TYPE_JSON, new Gson().toJson(registerUser)))
                .build();
        try {
            Response response = client.newCall(request).execute();

            if(response.isSuccessful()){
                RegistrationResponse registrationResponse =    new Gson().fromJson(response.body().string(),RegistrationResponse.class);
                setRegisteredId(registrationResponse.getUserId());

                EmoticupRegistrationResponse emoticupRegistrationResponse = new EmoticupRegistrationResponse();
                emoticupRegistrationResponse.setRegistrationResponse(registrationResponse);
                return  emoticupRegistrationResponse;
            }
            else{

                EmoticupRegistrationResponse emoticupRegistrationResponse = new EmoticupRegistrationResponse();
                emoticupRegistrationResponse.setThrowable("Registeration failed, Please try again after sometime");
                return emoticupRegistrationResponse;
            }
        }
        catch (IOException e) {

            EmoticupRegistrationResponse emoticupRegistrationResponse = new EmoticupRegistrationResponse();
            emoticupRegistrationResponse.setThrowable("Registeration failed, Please check internet connectivity");
            return emoticupRegistrationResponse;
        }

    }

    public static EmoticupRegistration getInstance(){

       return emoticupRegistration;

    }


    public String getRegisteredId() {
        return registeredId;
    }

    public void setRegisteredId(String registeredId) {
        this.registeredId = registeredId;
    }
}
