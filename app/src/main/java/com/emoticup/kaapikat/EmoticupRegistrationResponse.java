package com.emoticup.kaapikat;

/**
 * Created by fptech08 on 12/16/17.
 */

public class EmoticupRegistrationResponse {

    String errorMessage = "";

    RegistrationResponse registrationResponse;


    public void setThrowable(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setRegistrationResponse(RegistrationResponse registrationResponse) {
        this.registrationResponse = registrationResponse;
    }

    public RegistrationResponse getRegistrationResponse() {
        return registrationResponse;
    }

    public String getThrowable() {
        return errorMessage;
    }
}
