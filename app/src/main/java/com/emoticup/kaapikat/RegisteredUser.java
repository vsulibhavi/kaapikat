package com.emoticup.kaapikat;

/**
 * Created by fptech08 on 12/16/17.
 */

public class RegisteredUser {


    private String userId = "";
    private static RegisteredUser registeredUser = new RegisteredUser();

    private RegisteredUser(){


    }

    public static  RegisteredUser getInstance(){

        return registeredUser;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
}
