package com.emoticup.kaapikat;

import android.util.Log;

import com.ibm.mobilefirstplatform.clientsdk.android.push.api.MFPPushException;
import com.ibm.mobilefirstplatform.clientsdk.android.push.api.MFPPushResponseListener;

/**
 * Created by fptech08 on 12/17/17.
 */

class EventStreamListener implements MFPPushResponseListener<String> {


    @Override
    public void onSuccess(String response) {

        Log.i("device tagging ",response.toString());

    }

    @Override
    public void onFailure(MFPPushException exception) {

        Log.d("device tagging ",exception.getErrorMessage());
    }
}