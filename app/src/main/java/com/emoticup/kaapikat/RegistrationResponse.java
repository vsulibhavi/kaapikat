package com.emoticup.kaapikat;

/**
 * Created by fptech08 on 12/16/17.
 */

class RegistrationResponse {


    String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
