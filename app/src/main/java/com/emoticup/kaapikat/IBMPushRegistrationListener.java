package com.emoticup.kaapikat;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ibm.mobilefirstplatform.clientsdk.android.push.api.MFPPushException;
import com.ibm.mobilefirstplatform.clientsdk.android.push.api.MFPPushResponseListener;

/**
 * Created by fptech08 on 12/16/17.
 */

public class IBMPushRegistrationListener implements MFPPushResponseListener<String> {


    AppCompatActivity activity;
        IBMPushRegistrationListener(AppCompatActivity activity){
        this.activity = activity;

    }

        @Override
        public void onSuccess(final String response) {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("You are successfully registered");
                    alertDialog.show();
                    String[] resp = response.split("Text: ");

                    try {
                        org.json.JSONObject responseJSON = new org.json.JSONObject(resp[1]);
                        RegisteredUser.getInstance().setUserId(responseJSON.getString("userId"));
                    } catch (org.json.JSONException e) {
                        e.printStackTrace();
                    }

                    Log.i("YOUR_TAG_HERE", "Successfully registered for Bluemix Push Notifications with USER ID: " + RegisteredUser.getInstance().getUserId());

                }
            });

        }

        @Override
        public void onFailure(MFPPushException ex) {


            String errLog = "Error registering for Bluemix Push Notifications: ";
            String errMessage = ex.getErrorMessage();
            int statusCode = ex.getStatusCode();

            // Create an error log based on the response code and returned error message.
            if (statusCode == 401) {
                errLog += "Cannot authenticate successfully with Bluemix Push Notifications service instance. Ensure your CLIENT SECRET is correct.";
            } else if (statusCode == 404 && errMessage.contains("Push GCM Configuration")) {
                errLog += "Your Bluemix Push Notifications service instance's GCM/FCM Configuration does not exist.\n" +
                        "Ensure you have configured GCM/FCM Push credentials on your Bluemix Push Notifications dashboard correctly.";
            } else if (statusCode == 404) {
                errLog += "Cannot find Bluemix Push Notifications service instance, ensure your APP GUID is correct.";
            } else if (statusCode >= 500) {
                errLog += "Bluemix and/or the Bluemix Push Notifications service are having problems. Please try again later.";
            } else if (statusCode == 0) {
                errLog += "Request to Bluemix Push Notifications service instance timed out. Ensure your device is connected to the Internet.";
            }

            Log.e("YOUR_TAG_HERE", errLog);
        }
    }

