package com.emoticup.kaapikat;

/**
 * Created by fptech08 on 11/26/17.
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegisterUser {

    @JsonProperty("name")
    String name;

    @JsonProperty("_id")
    String _id;

    @JsonProperty("emailId")
    String emailId;


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
        this._id = emailId;
    }

    public String get_id() {
        return _id;
    }
}

